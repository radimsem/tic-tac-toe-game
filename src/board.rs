use std::vec;
use anyhow::{Result, bail};

use crate::enums::Direction;

pub struct Board {
    cols: usize,
    rows: usize,
    board: Vec<Vec<char>>,
}

impl Board {
    pub fn new(cols: usize, rows: usize) -> Self { 
        Self {
            cols, 
            rows,
            board: vec![vec!['_'; rows]; cols]
        } 
    }

    fn print_elems(&self, operation: impl Fn(usize, usize)) {
        for i in 0..self.cols {
            for j in 0..self.rows {
                operation(i, j);
            }
            println!();
        }
    }

    pub fn print_board(&mut self) {
        self.print_elems(|i, j| print!("{}", self.board[i][j]));
    }
    
    pub fn inform_cells_pos(&self) {
        self.print_elems(|i, j| print!("{}{} ", i, j));
    }

    pub fn insert(&mut self, pos_i: usize, pos_j: usize, side: char) {
        self.board[pos_i][pos_j] = side;
    }

    pub fn handle_valid_position(&self, pos_option: Option<i64>, pos_dir: &Direction) -> Result<usize> {
        if let Some(pos) = pos_option {
            if pos < 0 || pos > match pos_dir {
                Direction::COL => self.cols,
                Direction::ROW => self.rows
            } as i64 {
                bail!("Invalid position for this board!");
            }
            return Ok(pos as usize)
        }
        
        bail!("Position for both column and row is not set!");
    }

    pub fn handle_empty_pos(&self, pos_i: usize, pos_j: usize) -> bool {
        if self.board[pos_i][pos_j] != '_' {
            return false
        }

        true
    }

    pub fn handle_fullness(&self) -> bool {
        let target: Vec<bool> = vec![true; self.cols * self.rows];
        let mut full_cells: Vec<bool> = vec![];

        for i in 0..self.cols {
            for j in 0..self.rows {
                if self.board[i][j] != '_' {
                    full_cells.push(true);
                } else {
                    full_cells.push(false);
                }
            }
        }

        if full_cells == target {
            return true
        }

        false
    }

    pub fn handle_row_fill(&self, side: char) -> bool {
        let target: Vec<char> = vec![side; self.rows];

        for i in 0..self.cols {
            let mut row: Vec<char> = vec![];
            for j in 0..self.rows {
                row.push(self.board[i][j]);
            }
            if row == target {
                return true
            }
        }

        false
    }

    pub fn handle_col_fill(&self, side: char) -> bool {
        let target: Vec<char> = vec![side; self.cols];

        for i in 0..self.rows {
            let mut col: Vec<char> = vec![];
            for j in 0..self.cols {
                col.push(self.board[j][i]);
            }
            if col == target {
                return true
            }
        }

        false
    }

    pub fn handle_main_diagonal_fill(&self, side: char) -> bool {
        let target: Vec<char> = vec![side; self.cols];
        let mut main_diagonal: Vec<char> = vec![];

        for i in 0..self.cols {
            for j in 0..self.rows {
                if i == j {
                    main_diagonal.push(self.board[i][j]);
                }
            }
        }

        if main_diagonal == target {
            return true
        }

        false
    }

    pub fn handle_side_diagonal_fill(&self, side: char) -> bool {
        let target: Vec<char> = vec![side; self.cols];
        let mut side_diagonal: Vec<char> = vec![];

        for i in 0..self.cols {
            for j in 0..self.rows {
                if (i + j) == (self.cols - 1) {
                    side_diagonal.push(self.board[i][j]);
                }
            }
        }

        if side_diagonal == target {
            return true
        }

        false
    }
}