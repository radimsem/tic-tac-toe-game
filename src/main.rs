use game::Game;

mod enums;
mod board;
mod game;

const COLS: usize = 3;
const ROWS: usize = 3;

fn main() {
    let mut game = Game::new(COLS, ROWS);

    game.start();
}
