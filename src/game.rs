use core::panic;
use std::collections::HashMap;
use std::io::stdin;
use anyhow::Result;

use crate::board::Board;
use crate::enums::{Side, Direction};

pub struct Game {
    board: Board,
    o_counter: u8,
    x_counter: u8,
    results: HashMap<char, bool>
}

// public methods
impl Game {
    pub fn new(board_cols: usize, board_rows: usize) -> Self { 
        Self { 
            board: Board::new(board_cols, board_rows), 
            o_counter: 0, 
            x_counter: 0,
            results: HashMap::new()
        } 
    }

    pub fn start(&mut self) {
        println!("Tic-Tac-Toe Game!");
        println!();

        println!("Board with cell positions:");
        self.board.inform_cells_pos();

        println!();
        println!("Let's begin!");
        println!();

        while self.is_playable() {
            self.turn();
        }

        for result in self.results.iter() {
            if *result.1 == true {
                if *result.0 != 'D' {
                    println!("Side {} wins!:", *result.0);
                } else {
                    println!("The game ended in a draw, because there are no empty cells to fill!:");
                }
                self.board.print_board();
            }
        }
    }
}

// private methods
impl Game {
    fn is_playable(&mut self) -> bool {
        self.results.clear();

        self.results.insert(self.get_side_char(&Side::O), self.handle_side_fill(Side::O));
        self.results.insert(self.get_side_char(&Side::X), self.handle_side_fill(Side::X));

        for result in self.results.iter()  {
            if *result.1 == true {
                return false
            }
        }

        if self.board.handle_fullness() {
            self.results.clear();
            // D => draw
            self.results.insert('D', true);
            return false
        }

        true
    }

    fn turn(&mut self) {
        if self.o_counter == self.x_counter {
            self.play(&Side::O);
        } else if self.o_counter > self.x_counter {
            self.play(&Side::X)
        } else {
            panic!("X side has more plays than O!");
        }
    }

    fn play(&mut self, side_turn: &Side) {
        let mut input = String::new();

        println!("Current board:");
        self.board.print_board();
        println!();

        println!("{} side, enter position to fill: ", self.get_side_char(&side_turn));

        let _ = stdin().read_line(&mut input);
        println!();

        let position_results: [Result<usize>; 2] = self.get_positions(&input);
        let mut valid_positions: Vec<usize> = vec![];

        for pos_result in position_results {
            match pos_result {
                Ok(pos) => valid_positions.push(pos),
                Err(mess) => println!("{}", mess)
            }
        }

        if valid_positions.len() < 2 {
            println!("Try it again.");
            self.play(&side_turn);
        } else {
            let is_empty_pos = self.board.handle_empty_pos(valid_positions[0], valid_positions[1]);

            match is_empty_pos {
                true => {
                    println!("Filling cell {} with {}", input.trim(), self.get_side_char(&side_turn));
                    self.board.insert(valid_positions[0], valid_positions[1], self.get_side_char(&side_turn));
                    println!();

                    match side_turn {
                        Side::O => self.o_counter += 1,
                        Side::X => self.x_counter += 1
                    }        
                },
                false => {
                    println!("Cell {} is full, try it again!", input.trim());
                    println!();
                    self.play(&side_turn);
                }
            }
        }
    }

    fn get_side_char(&self, side: &Side) -> char {
        match side {
            Side::O => 'O',
            Side::X => 'X'
        }
    }
    
    fn get_spec_pos(&self, input: &str, dir: &Direction) -> Option<i64> {
        let mut curr_char: Option<char> = None;
        let mut input_chars = input.chars();
    
        for _ in 0..=match dir {
            Direction::COL => 0,
            Direction::ROW => 1
        } {
            curr_char = input_chars.next();
        }
    
        match curr_char {
            Some(pos_char) => Some(pos_char.to_string().parse::<i64>().unwrap()),
            None => None
        }
    }

    fn get_positions(&self, input: &str) -> [Result<usize>; 2] {
        [
            self.board.handle_valid_position(self.get_spec_pos(input, &Direction::COL), &Direction::COL),
            self.board.handle_valid_position(self.get_spec_pos(input, &Direction::ROW), &Direction::ROW),
        ]
    } 

    fn handle_side_fill(&self, side: Side) -> bool {
        let side_char = self.get_side_char(&side);
        let mut results: Vec<bool> = vec![];

        results.push(self.board.handle_col_fill(side_char));
        results.push(self.board.handle_row_fill(side_char));
        results.push(self.board.handle_main_diagonal_fill(side_char));
        results.push(self.board.handle_side_diagonal_fill(side_char));

        if results.contains(&true) {
            return true
        }

        false
    }
}