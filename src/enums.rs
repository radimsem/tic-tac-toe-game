pub enum Side {
    O,
    X
}

pub enum Direction {
    COL,
    ROW
}